import http from "./http";

function login(email: string, password: string) {
    console.log(email)
    return http.post('/auth/login', { email, password })
}
export default { login }