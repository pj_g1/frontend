import type { Ingredient } from '@/types/Ingredient'
import http from './http'

function formatDate(date: Date) {
  const day = String(date.getDate()).padStart(2, '0')
  const month = String(date.getMonth() + 1).padStart(2, '0')
  const year = date.getFullYear()
  return `${day}/${month}/${year}`
}

function addIngredient(ingredient: Ingredient & { files: File[] }) {
  const formData = new FormData()
  formData.append('name', ingredient.name)
  formData.append('price', ingredient.price.toString())
  formData.append('amount', ingredient.amount.toString())
  formData.append('Savedate', formatDate(ingredient.Savedate))
  if (ingredient.files && ingredient.files.length > 0) formData.append('file', ingredient.files[0])
  return http.post('/ingredients', formData, {
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
}

function updateIngredient(ingredient: Ingredient & { files: File[] }) {
  const formData = new FormData()
  formData.append('name', ingredient.name)
  formData.append('price', ingredient.price.toString())
  formData.append('amount', ingredient.amount.toString())
  if (ingredient.files && ingredient.files.length > 0) formData.append('file', ingredient.files[0])
  return http.post(`/ingredients/${ingredient.id}`, formData, {
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
}

function delIngredient(ingredient: Ingredient) {
  return http.delete(`/ingredients/${ingredient.id}`)
}

function getIngredient(id: number) {
  return http.get(`/ingredients/${id}`)
}

function getIngredients() {
  return http.get('/ingredients')
}

export default { addIngredient, updateIngredient, delIngredient, getIngredient, getIngredients }
