import type { Bill } from '@/types/Bill'
import http from './http'

function addBill(bill: Bill) {
  return http.post('/Bills', bill)
}

function updateBill(bill: Bill) {
  return http.patch('/Bills/' + bill.id, bill)
}

function delBill(bill: Bill) {
  return http.delete('/Bills/' + bill.id)
}

function getBill(id: number) {
  return http.get('/Bills/' + id)
}

function getBills() {
  return http.get('/Bills')
}

export default { addBill, updateBill, delBill, getBill, getBills }
