import type { Check } from "@/types/checkin";
import http from "./http";

function addNew(check: Check){
    return http.post('/check',check) 
}

function update(check: Check){
    return http.patch(`/check/${check.id}`, check);
}

function delCheck(check: Check){
    return http.delete(`/check/${check.id}`)
}

function getCheck(id: number){
    return http.get(`/check/${id}`)
}

function getChecks(id: number){
    return http.get('/check')
}

export default {addNew,update,delCheck,getCheck,getChecks}