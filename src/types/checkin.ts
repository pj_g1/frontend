type User = {
    id: number
    password: string
    fullname: string
    timein: string
    timeout: string
}

export type { User }