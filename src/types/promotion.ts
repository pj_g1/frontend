type Type = 'bath' | '%'
type Promotion = {
        id?: number
        name: string
        discount: number
        type: Type[]
}

export type { Promotion,Type}