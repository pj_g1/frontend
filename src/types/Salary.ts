type Salary = {
  id?: number
  date: string
  fullname: string
  workinghour: number
  workrate: number
  status: string
  salary: number
}

export type { Salary }
