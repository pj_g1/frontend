import { useLoadingStore } from './loading'
import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import ingredientService from '@/services/ingredient'
import type { Ingredient } from '@/types/Ingredient'
import { useMessageStore } from './message'

export const useIngredientStore = defineStore('ingredient', () => {
  const loadingStore = useLoadingStore()
  const messageStore = useMessageStore()
  const ingredients = ref<Ingredient[]>([])
  const SaveIngredient = ref<Ingredient[]>([])
  const initialIngredient: Ingredient & { files: File[] } = {
    name: '',
    price: 0,
    image: 'noimage.jpg',
    amount: 0,
    files: [],
    Savedate: new Date()
  }
  const editedIngredient = ref<Ingredient & { files: File[] }>(
    JSON.parse(JSON.stringify(initialIngredient))
  )

  async function getIngredient(id: number) {
    try {
      loadingStore.doLoad()
      const res = await ingredientService.getIngredient(id)
      editedIngredient.value = res.data
      loadingStore.finish()
    } catch (e: any) {
      console.log('eror')
      messageStore.showMessage(e.message)
      loadingStore.finish()
    }
  }
  async function getIngredients() {
    loadingStore.doLoad()
    const res = await ingredientService.getIngredients()
    ingredients.value = res.data
    loadingStore.finish()
  }
  async function saveIngredient() {
    try {
      loadingStore.doLoad()
      const ingredient = editedIngredient.value
      if (!ingredient.id) {
        // Add new
        console.log('Post ' + JSON.stringify(ingredient))
        const res = await ingredientService.addIngredient(ingredient)
      } else {
        // Update
        console.log('Patch ' + JSON.stringify(ingredient))
        const res = await ingredientService.updateIngredient(ingredient)
      }
      await getIngredients()
      loadingStore.finish()
    } catch (e: any) {
      messageStore.showMessage(e.message)
      loadingStore.finish()
    }
  }
  async function deleteIngredient() {
    try {
      loadingStore.doLoad()
      const ingredient = editedIngredient.value
      const res = await ingredientService.delIngredient(ingredient)
      await getIngredients()
      loadingStore.finish()
    } catch (e: any) {
      messageStore.showMessage(e.message)
      loadingStore.finish()
    }
  }

  const formatDate = (date: Date): string => {
    const day = String(date.getDate()).padStart(2, '0')
    const month = String(date.getMonth() + 1).padStart(2, '0')
    const year = date.getFullYear()
    return `${day}/${month}/${year}`
  }

  function copyIngredients() {
    console.log('Copy Ingredient')
    const currentDate = new Date()

    const formattedCurrentDate = formatDate(currentDate)

    const nextDay = new Date(currentDate)
    nextDay.setDate(nextDay.getDate() + 1)

    for (const i of ingredients.value) {
      if (i.Savedate.toString() == formatDate(nextDay)) {
        console.log(i.Savedate.toString())
        console.log(formatDate(nextDay))
        return 'Can save 1 time in 1 day!!!'
      }
    }

    const copiedIngredients = ingredients.value
      .filter((ingredient) => ingredient.Savedate.toString() === formattedCurrentDate)
      .map((ingredient) => ({
        id: undefined,
        name: ingredient.name,
        price: ingredient.price,
        amount: ingredient.amount,
        image: ingredient.image,
        Savedate: nextDay, // กำหนดวันที่และเวลาปัจจุบัน
        files: []
      }))
    ingredients.value.push(...copiedIngredients)

    // บันทึกข้อมูลที่คัดลอกลงในฐานข้อมูล
    copiedIngredients.forEach(async (ingredient) => {
      try {
        loadingStore.doLoad()
        const res = await ingredientService.addIngredient(ingredient)
        await getIngredients() // อัปเดตข้อมูลหลังจากบันทึกข้อมูลใหม่
        loadingStore.finish()
      } catch (e: any) {
        messageStore.showMessage(e.message)
        loadingStore.finish()
      }
    })
  }

  function clearForm() {
    editedIngredient.value = JSON.parse(JSON.stringify(initialIngredient))
  }

  return {
    ingredients,
    SaveIngredient,
    getIngredients,
    saveIngredient,
    deleteIngredient,
    editedIngredient,
    getIngredient,
    clearForm,
    copyIngredients
  }
})
