import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { Check } from '@/types/checkin'
import { useLoadingStore } from '@/stores/loading';
import checkService from '@/services/checkin'

export const useCheckStore = defineStore('check', () => {
    const loadingStore = useLoadingStore()
    const checks = ref<Check[]>([])
    const initialCheck: Check = {
        name: '',
        discount: 0,
        type: ['bath'],
    }
    const editedCheck = ref<Check>(JSON.parse(JSON.stringify(initialCheck)))
    
    async function getCheck(id: number) {
        loadingStore.doLoad()
        const res = await checkService.getCheck(id)
        editedCheck.value = res.data
        loadingStore.finish()
    }
    async function getChecks() {
        loadingStore.doLoad()
        const res = await checkService.getChecks()
        checks.value = res.data
        loadingStore.finish()
    }
    async function saveCheck(){
        const check = editedCheck.value
        loadingStore.doLoad()
        if(!check.id){
            console.log('Post' + JSON.stringify(check))
            const res = await checkService.addNew(check)
        }else {
            console.log('Patch' + JSON.stringify(check))
            const res = await checkService.update(check)
    
        }
        await getChecks()
        loadingStore.finish()
    }
    
    async function deleteCheck(){
        loadingStore.doLoad()
        const check = editedCheck.value
        const res = await checkService.delCheck(check)
        await getChecks()
        loadingStore.finish()
    }

    function clearForm() {
        editedCheck.value = JSON.parse(JSON.stringify(initialCheck))
    }
    return { checks, getChecks,saveCheck,deleteCheck,getCheck ,editedCheck ,clearForm}
})